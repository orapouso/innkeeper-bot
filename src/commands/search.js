const debug = require('debug')('innkeeper:search')
const Parser = require('rss-parser')
const stopwatch = require('../utils/stopwatch')()
const randResponse = require('../utils/rand').randResponse

const parser = new Parser()

const MAX_RESULTS = 3
const MATCH_BOUNDARY = 30
const MAX_RETRIES = 3

const FEED_URL = process.env.FEED_URL

const INITIAL_RESPONSES = [
  'Olá! Espera aí que eu já te digo se tem algum episódio com *${terms}*',
  'Você quer saber se tem algum episódio com *${terms}*??? TEM CERTEZA?? Espera um pouco que já te digo.',
  'Ahhhh!! Ninguém me deixa terminar de passar o café. Peraí que eu já te digo se tem algum episódio com *${terms}*',
  'Ih rapaz... Toma um café aí enquanto eu pego o livro com os episódios pra procurar se tem um com *${terms}*',
  'Já vai!! JÁ VAI!! É episódio pra caramba. Acha que é fácil encontrar algum com *${terms}*',
]

async function* handler(message, { terms } = {terms: ''}) {

  let response = 'Você não me deu nada pra pesquisar. Me dá essa cerveja aqui que já foi demais.'
  let termsReg = new RegExp(`(${terms}|${terms.replace(/\s+\S{1,3}(?!\S)|(?<!\S)\S{1,3}\s+/g, '').replace(/ /g, '|')})`, 'gi')
  debug('TERMS', terms)

  yield randResponse(INITIAL_RESPONSES, { terms })

  try {
    let feed
    let feedTries = 0
    while (++feedTries <= MAX_RETRIES && !feed) {
      try {
        debug('FEED TRY', feedTries)
        feed = await parser.parseURL(FEED_URL)
      } catch (e) {
        if (e.message.toLowerCase().includes('root')) {
          yield 'Calma aí! Ainda estou procurando o livro com todos os episódios'
        } else {
          throw e
        }
      }
    }

    if (!feed) {
      throw 'Infelizmente o podbean não respondeu o feed.xml. Tenta de novo ou mais tarde'
    }

    if (terms.length > 0) {
      stopwatch.reset()
      debug('SCORING STARTED')

      let count = 0
      let countExact = 0
      let scores = feed.items.map((item, i) => {
        let content = item['content:encodedSnippet']
        content = `${item.title}++${content.substring(0, content.indexOf('***'))}`
                  .replace(/(?:https?|ftp):\/\/[\n\S]+/g, '')
        termsReg.lastIndex = 0
        let matches = []
        let match, score = 0
        while ((match = termsReg.exec(content)) !== null) {
          score += match[0].split(/ /).length
          matches.push(match)
        }

        if (matches.length > 0) {
          count++
          countExact = countExact + (matches.some(m => m[0].toLowerCase() === terms) ? 1 : 0)
        }

        return {
          score,
          matches,
          i,
        }
      })
      .filter(item => item.matches && item.matches.length > 0)
      .sort((item1, item2) => {
        return item2.score - item1.score
      })
      .slice(0, MAX_RESULTS)
      .map((item) => {
        item.result = item.matches.map((m) => {
          return m.input
            .substring(Math.max(m.index - MATCH_BOUNDARY, 0), Math.min(m.index + m[0].length + MATCH_BOUNDARY, m.input.length))
            .replace(m[0], `*${m[0]}*`)
        }).join('...')

        return item
      })
      .map((item) => {
        return `
*${feed.items[item.i].title}*\n
${item.result}\n
${feed.items[item.i].link}
`})

      stopwatch()
      debug('FOUND', count, 'exacts', countExact)
      if (count > 0) {
        response = `AAEEE!! Olha aqui, olha aqui!! Encontrei *${count} episódios* com esses termos, dos quais *${countExact} exatos*. Aqui estão os *${Math.min(count, MAX_RESULTS)}* melhores\n\n${scores.join('\n\n')}`
      } else {
        response = `Olha... Infelizmente não consegui encontrar nenhum episódio com esses termos.`
      }
    }

    yield response
  } catch (e) {
    e = e.message || e
    yield `Ocorreu um erro inesperado: ${e}`
    return { error: true }
  }
}

async function* help() {
  yield `
Olha só! Eu entendi que você quer encontrar algum episódio, mas precisa me dizer o que procurar no livro. É episódio pra caramba!\n
Faz assim. Se quiser encontrar episódios de *hexcrawl* por exemplo:\n
/episodio hexcrawl\n
Se quiser encontrar o episódio número 217:
/episodio #217\n
Se quiser saber quais episódios tem participação de convidade, é só procurar pelo nome:
/episodio aline terumi\n\n
Agora testa aí!
`
}

module.exports = {
  handler,
  help,
}