const debug = require('debug')('inkeeper:help')

module.exports = async function* handler() {
    yield `
*O Estalajadeiro - Help*
Olá, sou *O Estalajadeiro* desta parte da cidade. Logo aqui ao lado tem uma ótima _dungeon_ para se aventurar. Enquanto se prepara para sua próxima incursão, porque não sentar e conversar com outros aventureiros? Em breve poderei lhes servir boas bebidas e iguarias locais. Por enquanto eu só recebo bem os mais novos e consigo começar uma conversa pra vocês. Fique a vontade!

*Comandos*
\`\`\`
/help ou /ajuda   - Mostra essa mensagem
/episodio {busca} - Pesquisa os 3 episódios que mais tenham a ver com a {busca}
/assuntos         - Lista os assuntos disponíveis
/papear           - Rola um dado para escolher um assunto
/rollose          - Cria nova ficha para um personagem de OSE 
/gerarpj
/rolloilfantasy   - Cria nova ficha para o projeto de OSE na megadungeon Cavernas Proibidas de Archaia
/rolaroilfantasy
\`\`\`
`
}
