const debug = require('debug')('innkeeper:topic')

let rand = (min, max) => {
    return Math.floor(Math.random()*(max-min+1)+min);
}

const TOPICS = 20
const TOTAL_LIST = require('../data/topics.json')
const D20 = parseInt(TOTAL_LIST.length / TOPICS)
const IDX = (new Date()).getDate() % D20
const LIST = TOTAL_LIST.slice(IDX * TOPICS, (IDX * TOPICS) + TOPICS).map((t, i) => `${i+1}. ${t}`)

async function* list(message) {
  debug('list topics')

  let resp = `
Olá!! Quer começar uma conversa por aqui? Que tal sortearmos um desses assuntos de hoje?
${LIST.join('\n')}

Para sortear um assunto me avise com */papear*
`
  yield resp
}

async function* chatter(message) {
  debug('chatter topics')

  let dice = rand(0, LIST.length)
  let resp = `
*PESSOAL!! PESSOAL!!* [${message.from.first_name} ${message.from.last_name||''}](tg://user?id=${message.from.id}) está querendo começar uma conversa. Deixa eu rolar um dadinho aqui:

rolando *1d${LIST.length}* 🎲

*Resultado:*
${LIST[dice]}
`
  yield resp
}

module.exports = {
  list,
  chatter,
}