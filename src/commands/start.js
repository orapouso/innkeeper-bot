const debug = require('debug')('inkeeper:start')

module.exports = async function* handler() {
  yield `You called /start command. This is an example message just to show that the bot is working`
}
