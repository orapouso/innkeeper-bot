const debug = require('debug')('inkeeper:new_member')
const randResponse = require('../utils/rand').randResponse

let rand = (min,max) => {
    return Math.floor(Math.random()*(max-min+1)+min);
}

const RESPONSES = [
  'Olá *${name}*. Sinta-se a vontade por aqui. Daqui a pouco te levo um café!',
  'Vejam só! Temos um novo viajante chegando! Já vou lhe servir uma cerveja. Digam olá para *${name}*',
  'Estávamos te esperando *${name}*!! Nos dê notícias da guerra!',
  'Preparem-se! *${name}* acaba de chegar!',
  'O que é isso *${name}*?! Quase não te vi entrar! Sente-se com os outros que já levo a sopa!',
]

module.exports = async function* handler(message) {
  let members = message.new_chat_members
  if (!members) {
    members = [message.from]
  }
  
  debug('new members', members)
  let plural = ''
  let last = members.pop()

  let name = `${last.first_name}`
  if (last.last_name) {
    name += ` ${last.last_name}`
  }

  if (members.length > 0) {
    plural = 's'
    name = `${members.map((m) => {
      let n = `${m.first_name}`
      if (m.last_name) {
        n += ` ${m.last_name}`
      }

      return n
    }).join(',')} e ${name}`
  }

  yield randResponse(RESPONSES, { name })
}
