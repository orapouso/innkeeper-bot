const debug = require('debug')('innkeeper:rolloilfantasy')
const rollose = require('../utils/roll_ose')

module.exports = async function* handler(message) {
    let attributes = rollose()

    let formmatedDices = attributes.map(attr => {
      return `${attr.name.toUpperCase()}) rolou (${attr.dices.join(' + ')}) = *${attr.total}*`
    })

    let from = `[@${message.from.first_name}](tg://user?id=${message.from.id})`

    yield `
~*NOVO AVENTUREIRO* entra na taverna~
 
- Ora, temos mais um interessado em se aventurar nas *Cavernas Proibidas de Archaia*!!

Vamos ver como ${from} se sai:
${formmatedDices.join('\n')}
`
}