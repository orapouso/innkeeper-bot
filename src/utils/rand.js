let rand = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

let randResponse = (responses, params) => {
  return Object.keys(params).reduce((response, param) => {
    console.log(new RegExp(`\\$\{${param}\}`, 'g'))
    return response.replace(new RegExp(`\\$\{${param}\}`, 'g'), params[param])
  }, responses[rand(0, responses.length - 1)])
}

module.exports = {
  rand,
  randResponse,
}