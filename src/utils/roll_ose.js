const debug = require('debug')('rollose')

let rand = (min, max) => {
  return Math.floor(Math.random()*(max-min+1)+min);
}

module.exports = () => {
  return [
    {name: 'str'}, {name: 'dex'}, {name: 'con'},
    {name: 'int'}, {name: 'wis'}, {name: 'cha'},
  ].map(attr => {
    attr.dices = Array(3).fill(0).map(() => {
      return rand(1, 6)
    })
    
    attr.total = attr.dices.reduce((t, c) => {
      return t + c
    }, 0)
    
    return attr
  })
}