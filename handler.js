const debug = require('debug')('telegrambotaaf')
const TelegramBot = require('./src/telegram')

const TOKEN = process.env.TELEGRAM_TOKEN
const bot = TelegramBot(TOKEN)

const start = require('./src/commands/start')
const help = require('./src/commands/help')
const join = require('./src/commands/join')
const topics = require('./src/commands/topic')
const rollOilFantasy = require('./src/commands/roll_oilfantasy')
const rollOSE = require('./src/commands/roll_ose')
const search = require('./src/commands/search')

module.exports.entry = async (event) => await bot(event)

bot.on(/\/start/, start)
bot.on(/\/(help|ajuda)/, help)

bot.onMemberJoin(join)

bot.on(/\/testjoin/, join)
bot.on(/\/(topics|assuntos)/, topics.list)
bot.on(/\/(chat|papear)/, topics.chatter)
bot.on(/\/(rolloilfantasy|rolaroilfantasy)/, rollOilFantasy)
bot.on(/\/(rollose|gerarpj)/, rollOSE)
bot.on(/\/(?:search|pesquisar|episodio) (?<terms>.*)/, search.handler)
bot.on(/\/(search|pesquisar|episodio)/, search.help)

bot.on('message', async function* (msg) {
  //TODO
  const chatId = msg.chat.id
  debug('generic message, ignoring')
  // await bot.sendMessage(chatId, 'Received your message')
})
